
const axios = require('axios');
const cheerio = require('cheerio');

const mongoose = require('mongoose');
////////////////////
const link = 'http://127.0.0.1:5500/%C4%90%E1%BB%8Ba%20%C4%91i%E1%BB%83m%20Caf%C3%A9_Dessert%20t%E1%BA%A1i%20Kon%20Tum.html'
const provinceNew = 62
const typeAdd = 0


mongoose.Promise = global.Promise;
const RESTAURANT_MODEL = mongoose.model('cafe', new mongoose.Schema({
    name: String,
    address: String,
    rating: Number,
    images: [String],
    province: Number,
    status: Number,
    modifyAt: Date,
    createAt: Date,
    amount: Number,
    appointmentStyle: [String],
    blogRef: [String],
    description: String,
    openCloseDescription: String,
    ratingTop100: Number,
    typeAddress: [Number],
    location: {
        type: { type: String },
        coordinates: [Number],
    },
    author: String,
    cost: {
        min: Number,
        max: Number
    },
    closed: Date,
    openTime: Date,
    amountComment: Number,
    amountImage: Number
}), 'cafe')

// Connect MongoDB at default port 27017.

const cheerIOLoadOptions = {
    withDomLvl1: true,
    normalizeWhitespace: false,
    xmlMode: false,
    decodeEntities: false
};

const reqSingle = async (URL, { name = '', rating = 0, image = 'noimage.jpg', amountComment, amountImage }) => {
    const resp = await axios.get(URL)
    const $ = cheerio.load(resp.data)
    const addresses = []
    $('.ldc-item-h-address > span').each((index, el) => {
        if ($(el).text().match(/[a-z]+/gi))
            addresses.push($(el).text())
    })

    const locations = addresses.map(address => {
        return new RESTAURANT_MODEL({
            name, rating, images: [image], address, province: provinceNew,
            status: 1,
            modifyAt: new Date(),
            createAt: new Date(),
            amount: 0,
            appointmentStyle: [],
            blogRef: [],
            description: '',
            openCloseDescription: 'Cả tuần',
            ratingTop100: 100,
            typeAddress: [typeAdd],
            location: {
                type: 'Point',
                coordinates: [0, 0]
            },
            author: null,
            cost: {
                min: 0,
                max: 0
            },
            closed: new Date(),
            openTime: new Date(),
            amountComment, amountImage
        }).save()
    })

    await Promise.all(locations).then(res => console.log('bbbb'))
}

const reqFunc = async (MAIN_URL = '') => {

    await mongoose.connect('mongodb://localhost:27017/data_crawler', {
        useNewUrlParser: true,
        useCreateIndex: true,
    }, async (err) => {
        if (!err) {
            console.log('MongoDB Connection Succeeded.')
        } else {
            console.log('Error in DB connection: ' + err)
        }
    });

    const resp = await axios.get(link)

    const $ = cheerio.load(resp.data, cheerIOLoadOptions)

    const dataCols = $('.filter-result-item')
    const elements = []
    dataCols.each((index, el) => { elements.push(el) })


    for (let i = 0; i < elements.length; i++) {
        const select = cheerio.load($(elements[i]).html(), cheerIOLoadOptions)
        const name = select('.resname h2 a').text()
        const rating = parseFloat(select('.status div').text()) / 2 || 0
        const address = select('.result-address > .address > span').text().replace(/\n/g, '').trim()
        const imageDir = (select('.result-image img').attr('src') || '/noimage.jpg').split('/')

        let amountComment = (select('.stats a span[data-bind="text: TotalReview.formatK(1)"]').text()) || '0'
        let amountImage = (select('.stats a span[data-bind="text: PictureCount.formatK(1)"]').text()) || '0'


        amountComment = amountComment.match(/k/) ? parseFloat(amountComment) * 1000 : parseFloat(amountComment)
        amountImage = amountImage.match(/k/) ? parseFloat(amountImage) * 1000 : parseFloat(amountImage)


        if (!name || !address || !select) {
            console.log('wrong select, continue ...')
            continue;
        }
        if (address.match(/chi nhánh/)) {
            console.log('---------- Nhiều chi nhánh ------------')
            await reqSingle(select('.resname h2 a').attr('href'), { name, rating, image: imageDir[imageDir.length - 1], amountComment, amountImage })
        } else {
            const restaurant = new RESTAURANT_MODEL({
                name, rating, address, images: [imageDir[imageDir.length - 1]], province: provinceNew, amountComment, amountImage,
                status: 1,
                modifyAt: new Date(),
                createAt: new Date(),
                amount: 0,
                appointmentStyle: [],
                blogRef: [],
                description: '',
                openCloseDescription: 'Cả tuần',
                ratingTop100: 100,
                typeAddress: [typeAdd],
                location: {
                    type: 'Point',
                    coordinates: [0, 0]
                },
                author: null,
                cost: {
                    min: 0,
                    max: 0
                },
                closed: new Date(),
                openTime: new Date()
            })
            await restaurant.save()
            console.log('aaaa')
        }
    }
    console.log('done')
}




reqFunc();



